package dz22;

import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;


import shop.OversizePage;


public class OversizeTest {
    private WebDriver driver;
    private OversizePage oversizePage;
    protected String baseUrl = "https://homebrandofficial.ru/wear";

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        driver.get(baseUrl);
        oversizePage = new OversizePage(driver);
    }

        @After
    public void tearDown(){
        driver.quit();
    }
    @Test
    public void testSecond() {
        SoftAssertions softAssertions = new SoftAssertions();
        String name = "Иванов Иван Иванович";
        String phone = "(000) 000-00-00";
        String address = "Москва, Королева 10";

        oversizePage.oversize()
                .addCart()
                .cart()
                .placeOrder()
                .fillPersonalField(name, phone, address)
                .fillDetail(name)
                .streetField()
                .houseField()
                .clickButton();

//        Проверки
        String underLineText ="Надпись под полем 'Телефон' не соответствует требованию";
        String checkLine = oversizePage.checkUnderLine.getText();
        softAssertions.assertThat(checkLine).as(underLineText)
                .isEqualTo("Укажите, пожалуйста, корректный номер телефона");

        String pageText = "Надпись внизу страницы не соответствует требованию";
        String checkPage = oversizePage.checkUnderPage.getText();
        softAssertions.assertThat(checkPage).as(pageText)
                .isEqualTo("Укажите, пожалуйста, корректный номер телефона");

        softAssertions.assertAll();
    }
}
