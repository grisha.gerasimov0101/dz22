package dz22;

import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import shop.LongsleevePage;

import java.util.concurrent.TimeUnit;


public class LongsleeveTest {
    private WebDriver driver;
    protected String baseUrl = "https://homebrandofficial.ru/wear";
    private LongsleevePage longsleevePage;
    @Before
    public void setUp() {
        driver = new ChromeDriver();
        driver.get(baseUrl);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().window().fullscreen();
        longsleevePage = new LongsleevePage(driver);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testFirst() {
        SoftAssertions softAssertions = new SoftAssertions();
        String nameElement = "Лонгслив White&Green";
        longsleevePage.setSearch(nameElement)
                .buttonSearchHome();

//        Проверки

        String nameElementPage = longsleevePage.pageName.getText();
        softAssertions.assertThat(nameElementPage).as("Название не соответствует требованию")
                .isEqualToIgnoringCase(nameElement);

        String costElement = "2 800";
        String costElementPage = longsleevePage.pageCost.getText();
        softAssertions.assertThat(costElementPage).as("Цена не соответствует требованию")
                .isEqualToIgnoringCase(costElement);

        int expectedQuantityElement = 1;
        int quantityElement = longsleevePage.pageQuantity.size();
        softAssertions.assertThat(quantityElement).as("Количество товаров на странице не соответствует требованию")
                .isEqualTo(expectedQuantityElement);

        softAssertions.assertAll();
    }
}

