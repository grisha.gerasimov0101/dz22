package shop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class OversizePage {

    @FindBy(xpath = "//*[@ href=\"https://homebrandofficial.ru/tproduct/544765431-930803998551-futbolka-oversize\" ]")
    private WebElement oversizeElement;

    @FindBy(xpath = "//*[@ class=\"js-store-prod-popup-buy-btn-txt\"]")
    private WebElement addCartElement;

    @FindBy(xpath = "//*[@ class=\"t706__carticon-img\"]")
    private WebElement cartElement;

    @FindBy(xpath = "//div[@class=\"t706__sidebar-bottom\"]//button")
    private WebElement placeOrderElement;

    @FindBy(id = "input_1496239431201")
    private WebElement nameFieldElement;

    @FindBy(xpath = "//input[@ style=\"color: rgb(94, 94, 94);\"]")
    private WebElement phoneFieldElement;

    @FindBy(id = "input_1630305196291")
    private WebElement addressFieldElement;

    @FindBy(xpath = "//input[@ placeholder=\"Иванов Иван Иванович\"]")
    private WebElement addresseeFieldElement;

    @FindBy(xpath = "//input[@ name=\"tildadelivery-street\"]")
    private WebElement streetCursorElement;

    @FindBy(xpath = "//div[@ data-shortname=\"г Москва\"]")
    private WebElement streetFieldVarElement;

    @FindBy(xpath = "//input[@ name=\"tildadelivery-house\"]")
    private WebElement houseCursorElement;

    @FindBy(xpath = "//div[@ data-name=\"д. 77, корп. 2, стр. 3\"]")
    private WebElement houseFieldVarElement;

    @FindBy(xpath = "//div[@class=\"t706__cartpage-form t-col t-col_6\"]//button")
    private WebElement buttonElement;

    @FindBy(id = "error_1496239478607")
    public WebElement checkUnderLine;
    @FindBy(xpath = "//*[@id=\"form561378404\"]/div[2]//p[5]")
    public WebElement checkUnderPage;

    private WebDriver driver;
    private WebDriverWait wait;

    public OversizePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver,5L);
        PageFactory.initElements(driver, this);
    }

    public OversizePage oversize() {
        oversizeElement.click();
        return this;
    }

    public OversizePage addCart() {
        addCartElement.click();
        return this;
    }

    public OversizePage cart() {
        cartElement.click();
        return this;
    }

    public OversizePage placeOrder() {
        placeOrderElement.click();
        return this;
    }

    public OversizePage fillPersonalField(String name, String phone, String address) {
        nameFieldElement.sendKeys(name);
        phoneFieldElement.click();
        phoneFieldElement.sendKeys(phone);
        addressFieldElement.sendKeys(address);
        return this;
    }

    public OversizePage fillDetail(String name) {
        addresseeFieldElement.sendKeys(name);
        return this;
    }

    public OversizePage streetField() {
        streetCursorElement.click();
        wait.until(ExpectedConditions.visibilityOf(streetFieldVarElement));
        streetFieldVarElement.click();
        return this;
    }

    public OversizePage houseField() {
        houseCursorElement.click();
        wait.until(ExpectedConditions.visibilityOf(houseFieldVarElement));
        houseFieldVarElement.click();
        return this;
    }

    public OversizePage clickButton() {
        buttonElement.click();
        return this;
    }

}
