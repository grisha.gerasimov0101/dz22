package shop;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class LongsleevePage {

    @FindBy(xpath = "//*[@ placeholder=\"Поиск\"]")
    private WebElement searchField;

    @FindBy(xpath = "//*[@ class=\"t-store__search-icon js-store-filter-search-btn\"]")
    private WebElement searchButton;

    @FindBy(xpath = "//*[@ class=\"js-store-prod-name js-product-name t-store__card__title t-name t-name_md\"]")
    public WebElement pageName;

    @FindBy(xpath = "//*[@ class=\"js-product-price js-store-prod-price-val t-store__card__price-value notranslate\"]")
    public WebElement pageCost;

    @FindBy(xpath = "//*[@ class=\"js-product-img t-store__card__bgimg t-store__card__bgimg_hover t-bgimg loaded\"]")
    public List <WebElement> pageQuantity;

    private WebDriver driver;

    public LongsleevePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public LongsleevePage setSearch(String search){
        searchField.sendKeys(search);
        return this;
    }

    public LongsleevePage buttonSearchHome(){
        searchButton.click();
        return this;
    }

}
